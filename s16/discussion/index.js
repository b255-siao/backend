console.log('Hello World');

// Arithmetic Operators:
let x = 1397;
let y = 7831;
let sum = x + y;
console.log('Result of addition operator: ' + sum);

let difference = x - y;
console.log('Result of subtraction operator: ' + difference);

let product = x * y;
console.log('Result of multiplication operator: ' + product);

let quotient = x / y;
console.log('Result of division operator: ' + quotient);

let remainder = y % x;
console.log('Result of modulo operator: ' + remainder);

// Assignment Operators:
let assignmentNumber = 8;

// Addition assignment operator(+=):
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
assignmentNumber = assignmentNumber + 2;
console.log('Result of addition assignment operator: ' + assignmentNumber);

// Shorthand
assignmentNumber += 2;
console.log('Result of addition assignment operator: ' + assignmentNumber);

assignmentNumber -= 2;
console.log('Result of subtraction assignment operator: ' + assignmentNumber);

assignmentNumber *= 2;
console.log('Result of multiplication assignment operator: ' + assignmentNumber);

assignmentNumber /= 2;
console.log('Result of division assignment operator: ' + assignmentNumber);

// Multiple operators and parentheses
/*
  - When multiple operators are applies in a single statement, it follows the PEMDAS
*/

let mdas = 1 + 3 - 3 * 4 / 5;
console.log('Result of mdas operations: ' + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log('Result of pemdas operation: ' + pemdas);

// Increment and decrement
let z = 1;
let increment = ++z;
console.log('Result of increment: ' + increment);

let decrement = --z;
console.log('Result of decrement: ' + decrement);

// Type coercion:
/* 
  - Type coercion is the automatic or implicit conversion of values from one data type to another.
  - This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
  - Values are automatically converteed from one data type to another in order to resolve operation.
*/
let numA = '10';
let numB = 12;

// adding/concatenating a string and a number will result in a string

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison operators
let juan = 'juan';

// Equality operator(==)
/*
  - Checks whether the operands are equal/have the same content
  - Returns a boolean value
*/
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
// Compares two strings that are the same
console.log('juan' == 'juan');
// Compares a string with the variable 'juan' declared above
console.log('juan' == juan);

// Inequality operator
/* 
  - Checks whether the operands are not equal/have different content
*/
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);

// Strict equality operator
/* 
  - Checks whether the operands are equal/have the same content
  - Also COMPARES the data types of 2 values
  - JS is a loosely typed language meaning that values of different data types can be stored in variables
  - This sometimes can cause problems within our code
*/
console.log('Strict equality operator');
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// strict inequality operator
console.log('strict inequality operator');
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Relational operators

// Some comparison operators check whether one value is greater or less than to the other value
let a = 50;
let b = 65;

// GT or greater than symbol(>)
let isGreaterThan = a > b;
// LT or less than symbol(<)
let isLessThan = a < b;
// GTE or greater than or equal to(>=)
let isGTorEqual = a >= b;
// LTE or less than or equal to(<=)
let isLTorEqual = a <= b;

console.log('comparison operators');
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND operator(&& - Double ampersand)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log('Result of logical AND operator: ' + allRequirementsMet);

// Logical OR operator(|| - Double pipe)
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log('Result of logical OR operator: ' + someRequirementsMet);

// Logical NOT operator(! - Exclamation point)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log('Result of logical NOT operator: ' + someRequirementsNotMet);