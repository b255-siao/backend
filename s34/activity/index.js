const express = require('express');

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get('/home', (req, res) => {
  res.send('Welcome to the home page');
});

let users = []
app.get('/users', (req, res) => {
  users.push(req.body);
  res.send([req.body]);
})

app.delete('/delete-user', (req, res) => {
  let message;

  for(let i = 0; i < users.length; i++) {
    if(req.body.users == users[i].users) {
      users.splice(i, 1);
      message = `User ${req.body.username} has been deleted`;
      break;
    };
  };
  if (message == undefined) {
    message = 'User does not exist';
  }
  // if (users.length === 0) {
  //   message = 'No users found';
  // };

  res.send(message);
  console.log(users);
})

if(require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}`));
};

module.exports = app;