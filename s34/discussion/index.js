// use the 'require' directive to load the express module/package
// a 'module' is a software component or a part of a program  that contains one or more routines
// this is used to get the content of the express package to be used by our application
// it allow us access the methods and functions that will allow us to create a server
const express = require('express');

// create an application using express.
// this creates an express application and stores this in a constant called app.
// in layman's terms app is our server.
const app = express();

// for our application server to run, we need a port to listen
const port = 4000;

// setup for allowing the server to handle data from requests.
// allows your app to read json data.
// methods used from express JS are middleware.
// middleware is a software that provides common services and capabilities to applications outside of what's 
// offered by the operating system
app.use(express.json());

// allows your app to read data from forms
// by default, information received from the URL can only be received as a string or an array.
// by applying the option for "extended:true" this allows us to receive information in other data types such
// as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes
// express has a methods corresponding to each HTTP method
app.get("/", (req, res) => {
  res.send("hello world!");
});

// Create another GET route
app.get('/hello', (req, res) => {
  res.send('Hello from the /hello endpoint!');
});

// Create a POST route
app.post('/hello', (req, res) => {
  // req.body contains the contents/data of the request body
  // all the properties 
  res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})


// an array that will store user objects when the "/signup" route is accessed
// this will served as  our mock database
let users = [];
app.post('/signup', (req, res) => {
  console.log(req.body);

  if(req.body.userName !== '' && req.body.password !== '') {
    users.push(req.body);
    
    // if contents of the 'request.body' with the property 'userName' and 'password' is not empty
    res.send(`User ${req.body.userName} successfully registered!`);
  } else {
    res.send('please input BOTH username and password');
  };
});


// Create PUT route to change password of a specific user
app.put('/change-password', (req, res) => {
  let message;

  // creates for loop that will loop through each element of the array
  for(let i = 0; i < users.length; i++) {
    // if the username provided in the postman client and the username of the current object in the loop is the same
    if(req.body.userName == users[i].userName) {
      // changes password of the user found by the loop
      users[i].password = req.body.password;

      message = `User ${req.body.userName}'s password has been updated`;
      // breaks out of the loop once a user that matches the username provided is found 
      break;
    } else {
      message = 'User does not exist';
    };
  };
  res.send(message);
  console.log(users);
});


// tells our server to listen to the port.
// if the port is accessed, we can run the server.
// returns a message to confirm that the server is running in the terminal

// if(require.main) would allow us to listen to the app directly if it is not imported to another module, 
// it will run the app directly.
// else, if it is needed to be imported, it will not run the app and instead export it to be used in another file.
if(require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}`));
};

module.exports = app;