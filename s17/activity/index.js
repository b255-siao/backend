/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/
function printUserInfo() {
  let printFullName = prompt('Enter your Full Name: ');
  let printAge = prompt('Enter your age: ');
  let printLocation = prompt('Enter your location: ');
  let printCat = prompt("Enter your cat's name");
  let printDog = prompt("Enter your dog's name");

  console.log('printUserInfo()');
  console.log("Hi, I'm " + printFullName + ".");
  console.log('I am ' + printAge + ' years old.');
  console.log('I live in ' + printLocation);
  console.log('I have a cat named ' + printCat + '.');
  console.log('I have a dog named ' + printDog + '.');
};
printUserInfo();

function printFiveBands() {
  let firstBand = 'The Beatles';
  let secondBand = 'Taylor Swift';
  let thirdBand = 'The Eagles';
  let fourthBand = 'Rivermaya';
  let fifthBand = 'Eraserheads';
  
  console.log('printFiveBands()');
  console.log(firstBand);
  console.log(secondBand);
  console.log(thirdBand);
  console.log(fourthBand);
  console.log(fifthBand);
};
printFiveBands();

function printFiveMovies() {
  let firstMovie = "Lion King";
  let secondMovie = "Howl's Moving Castle";
  let thirdMovie = "Meet the Robinsons";
  let fourthMovie = "School of Rock";
  let fifthMovie = "Sprited Away";

  console.log('printFiveMovies()');
  console.log(firstMovie);
  console.log(secondMovie);
  console.log(thirdMovie);
  console.log(fourthMovie);
  console.log(fifthMovie);
};
printFiveMovies();

function printFriends() {
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

  console.log('printFriends()');
	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();



//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}
