console.log('hello world!');

// [SECTION] Getting all posts

// The Fetch API allows you to asynchronously requests for a resource(data)
// A "promise" is an object that represents the eventual completion(or failure) of an asynchronous function and its resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Retrieval all posts following the REST API
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
  // the fetch method will return a promise that resolves to a response object
  // the then method captures the response object and returns another promise which will be eventually resolve or rejected
  .then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
  // use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
  .then((response) => response.json())
  // print the converted JSON value from the "fetch" request
  // using multiple "then" methods creates a "promise chain"
  .then((json) => console.log(json));

// the "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// used in functions to indicate which portions of code should be waited for
// creates an asynchronous function
async function fetchData() {
  // waits for the "fetch" method to complete then stores the value in the result variable
  let result = await fetch('https://jsonplaceholder.typicode.com/posts');

  // result returned by fetch returns a promise
  console.log(result);
  // the returned response is object
  console.log(typeof result);
  // we cannot access the content of response by directly accessing its body property
  console.log(result.body);

  // converts the data from the "response" object as JSON
  let json = await result.json();
  // print out the content of the "response" object
  console.log(json);
};
fetchData();

// [SECTION] Getting a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1')
  .then((response) => response.json())
  .then((json) => console.log(json))

// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  // sets the method of the request object to be sent to the backend
  // specified that the content will be in a JSON structure
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'New Post',
    body: 'Hello World',
    userId: 1
  })
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Update a post using PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1')
  .then((response) => response.json())
  .then((json) => console.log('put', json))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    id: 1,
    title: 'UPDATE Post',
    body: 'Hello AGAIN',
    userId: 1
  })
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post using PATCH
// Updates a specific post following the REST API
// The difference between PUT and PATCH is the number of properties being changed
// Patch is used to update the whole object
// PUT is used to update a single/several properties
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Corrected post',
  })
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Deleting a post
// Deleting a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
});

// [SECTION] Filtering posts
// the data can be filtered by sending the userID along with the URL
// Information sent via the url can be done by adding the question mark symbol(?)
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
  .then((response) => response.json())
  .then((json) => console.log('json', json));

// [SECTION] Retrieving comments for a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
  .then((response) => response.json())
  .then((json) => console.log('comments', json));