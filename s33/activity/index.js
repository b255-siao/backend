fetch('https://jsonplaceholder.typicode.com/posts/')
.then((response) => response.json())
.then((json) => console.log(json.map((item) => {
  return item.title
})));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json'
  }
})
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts/', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Created to do list item',
    userId: 1
  })
})
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    dateCompleted: 'Pending',
    description: 'To update the my to do list with a different data structure',
    id: 1,
    status: 'Pending',
    title: 'Updated to do list item',
    userId: 1  
  })
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    completed: false,
    dateCompleted: '07/09/21',
    id: 1,
    status: 'Complete',
    title: 'Delectus aut autem',
    userId: 1
  })
})
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
});

