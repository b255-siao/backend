const express = require('express');

const mongoose = require('mongoose');
const app = express();

const port = 3001;

mongoose.connect("mongodb+srv://kingsiao-255:Admin123@cluster0.mtbwzq2.mongodb.net/test", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log("We're connected to the cloud database"));

const userSchema = new mongoose.Schema({
  username: String,
  password: String
  // status: {
  //   type: String,
  //   default: "pending"
  // }
})

const User = mongoose.model("User", userSchema)

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post('/signup', (req, res) => {
  User.findOne({username: req.body.username, password: req.body.password}).then((result, err) => {
    if(result != null && result.username ==  req.body.username && result.password ==  req.body.password) {
      return res.send('Duplicate Username Found');
    } else {
      let newUser = new User({
        username : req.body.username,
        password : req.body.password
      })

      newUser.save().then((savedUser, saveErr) => {
        if(saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send('New User Created');
        };
      });
    };
  });

  // User.findOne({password: req.body.password}).then((result, err) => {
  //   if(result != null && result.password ==  req.body.password) {
  //     return res.send('Duplicate Password Found');
  //   } else {
  //     let newUser = new User({
  //       password : req.body.password
  //     })

  //     newUser.save().then((savedUser, saveErr) => {
  //       if(saveErr) {
  //         return console.error(saveErr);
  //       } else {
  //         return res.status(201).send('New User Created');
  //       };
  //     });
  //   };
  // });
});


if(require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}`));
};

module.exports = app;