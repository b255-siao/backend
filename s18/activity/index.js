/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/

function addNum(num1, num2) {
  let sum = num1 + num2;
  
  console.log('Displayed sum of ' + num1 + ' and ' + num2);
  console.log(sum);
};
addNum(5, 15);

function subNum(num1, num2) {
  let difference = num1 - num2;
  
  console.log('Displayed difference of ' + num1 + ' and ' + num2);
  console.log(difference);
};
subNum(20, 5);


function multiplyNum(num1, num2) {
  let product =  num1 * num2;;

  console.log('The product of ' + num1 + ' and ' + num2 + ':');
  console.log(product);
}
multiplyNum(50, 10);

function divideNum(num1, num2) {
  let quotient = num1 / num2;

  console.log('The quotient of ' + num1 + ' and ' + num2 + ':');
  console.log(quotient);
};
divideNum(50, 10);

function getCircleArea(radius, exponent) {
  let circleArea = (radius ** exponent) * 3.1416;
  console.log('The result of getting the area of a circle with ' + radius + ' radius:');
  console.log(circleArea);
}
getCircleArea(15, 2);

function getAverage(num1, num2, num3, num4) {
  let averageVar = (num1 + num2 + num3 + num4) / 4;
  // let averageVar = 4;
  // console.log(averageVar);
  console.log('The average of ' + num1 + ', ' + num2 + ', ' + num3 + ', ' + num4 + ':');
  console.log(averageVar);
}
getAverage(20, 40, 60, 80);

function checkIfPassed(score, totalScore) {
  let getAverage = score / totalScore * 100;
  let isPassingScore = getAverage >= 75; 
  console.log('Is ' + score + '/' + totalScore + ' a passing score?');
  console.log(isPassingScore);
}
checkIfPassed(38, 50);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
