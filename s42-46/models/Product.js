const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is required"]
  },
  description: {
    type: String,
    required: [true, "Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  is_active: {
    type: String,
    default: true
  },
  date_created: {
    type: Date,
    default: new Date()
  },
  user_orders: [
    {
      user_id: {
        type: String,
        required: [true, "User ID is required"]
      }
    }
  ]
})

module.exports = mongoose.model("Product", productSchema)