const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "User is required"]
  },
  password: {
    type: String,
    required: [true, "Password is required"]
  },
  is_admin: {
    type: Boolean,
    default: false
  },
  ordered_product: [
    {
      products: [
        {
          product_id: {
            type: String,
            required: [true, "Product ID is required"]
          },
          product_name: {
            type: String,
            required: [true, "Product Name is required"]
          },
          quantity: {
            type: Number,
            default: 1
          }
        }
      ],
      total_amount: {
        type: Number,
        default: 0
      },
      purchased_on: {
        type: Date,
        default: new Date()
      }
    }
  ]
});

module.exports = mongoose.model("User", userSchema);