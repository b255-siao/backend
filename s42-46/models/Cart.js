const mongoose = require("mongoose")

const cartSchema = new mongoose.Schema({
  user_id: {
    type: String,
    required: [true, "User ID is required"]
  },
  product_id: {
    type: String,
    required: [true, "Product ID is required"]
  },
  quantity: {
    type: Number,
    required: [true, "Quantity is required"]
  },
  subtotal: {
    type: Number,
    required: [true, "Subtotal is required"]
  },
  total_price: {
    type: Number,
    required: [true, "Total Price is required"]
  }
})

module.exports = mongoose.model("Cart", cartSchema)