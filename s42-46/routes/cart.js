const express = require("express")
const router = express.Router()
const cartController = require("../controllers/cart")
const auth = require("../auth")

router.post("/createCart", (req, res) => {
  cartController.addProductsToCart(req.body)
    .then(
      result => res.send(result)
    )
})

router.put("/:cart_id", (req, res) => {
  cartController.updateCart(req.params, req.body).then(
    result => res.send(result)
  )
})

router.delete("/:cart_id", (req, res) => {
  cartController.deleteCart(req.params).then(
    result => res.send(result)
  )
})

router.get("/retrieveAllCarts", auth.verify, (req, res) => {
  cartController.getAllCarts()
    .then(
      result => res.send(result)
    )
})

module.exports = router