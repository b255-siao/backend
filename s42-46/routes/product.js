const express = require("express")
const router = express.Router()
const productController = require("../controllers/product")
const auth = require("../auth")

router.post("/createProduct", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization)
  if (userData.is_admin == true) {
    productController.createProduct(req.body)
      .then(
        result => res.send(result)
      )
  } else {
    res.send(false)
  }
})

router.get("/retrieveAllProducts", auth.verify, (req, res) => {
  productController.getAllProducts()
    .then(
      result => res.send(result)
    )
})

router.get("/retrieveAllActiveProducts", auth.verify, (req, res) => {
  productController.getAllActive()
    .then(
      result => res.send(result)
    )
})

router.get("/:product_id", auth.verify, (req, res) => {
  productController.getSingleProduct(req.params)
    .then(
      result => res.send(result)
    )
})

router.put("/:product_id", auth.verify, (req, res) => {
  productController.updateProduct(req.params, req.body)
    .then(
      result => res.send(result)
    )
})

router.patch("/:product_id/archive", auth.verify, (req, res) => {
  productController.archiveProduct(req.params)
    .then(
      result => res.send(result)
    )
})

router.put("/:product_id/activate", auth.verify, (req, res) => {
  productController.activateProduct(req.params)
    .then(
      result => res.send(result)
    )
})

module.exports = router