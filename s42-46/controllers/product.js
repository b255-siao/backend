const Product = require("../models/Product")

// Create Product (Admin only)
const createProduct = (reqBody) => {
  let newProduct = new Product({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  })
  return newProduct.save().then((product, error) => {
    if (error) {
      return 'Unexpected Error. Try again later.'
    } else {
      return 'Your changes have been successfully saved!'
    }
  })
}

// Retrieve all products
const getAllProducts = () => {
  return Product.find({}).then(result => {
    return result
  })
}

// Retrieve all active products
const getAllActive = () => {
  return Product.find({is_active: true}).then(result => {
    return result
  })
}

// Retrieve single product
const getSingleProduct = (reqParams) => {
  return Product.findById(reqParams.product_id).then(result => {
    return result
  })
}

//  Update Product information (Admin only)
const updateProduct = (reqParams, reqBody) => {
  let updateProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  }
  return Product.findByIdAndUpdate(reqParams.product_id, updateProduct).then((product, error) => {
    if (error) {
      return 'Unexpected Error. Try again later.'
    } else {
      return 'Your changes have been successfully saved!'
    }
  })
}

// Archive Product (Admin only)
const archiveProduct = (reqParams) => {
  let updateActiveField = {
    is_active: false
  }
  return Product.findByIdAndUpdate(reqParams.product_id, updateActiveField).then((product, error) => {
    if(error) {
      return 'Unexpected Error. Try again later.'
    } else {
      return 'Your changes have been successfully saved!'
    }
  })
}

// Activate Product (Admin only) 
const activateProduct = (reqParams) => {
  let updateActiveField = {
    is_active: true
  }
  return Product.findByIdAndUpdate(reqParams.product_id, updateActiveField).then((product, error) => {
    if (error) {
      return 'Unexpected Error. Try again later.'
    } else {
      return 'Your changes have been successfully saved!'
    }
  })
}

module.exports = {
  createProduct,
  updateProduct,
  getAllActive,
  getAllProducts,
  getSingleProduct,
  updateProduct,
  archiveProduct,
  activateProduct
}