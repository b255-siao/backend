const Cart = require("../models/Cart")

const addProductsToCart = (reqBody) => {
  let newCart = new Cart({
    user_id: reqBody.user_id,
    product_id: reqBody.product_id,
    quantity: reqBody.quantity,
    subtotal: reqBody.subtotal,
    total_price: reqBody.quantity * reqBody.subtotal
  })
  return newCart.save().then((cart, error) => {
    if(error) {
      return 'Unexpected Error'
    } else {
      return 'Your product has been added to your cart.'
    }
  })
}

const updateCart = (reqParams, reqBody) => {
  let updateCartQuantity = {
    quantity: reqBody.quantity,
    subtotal: reqBody.subtotal,
    total_price: reqBody.quantity * reqBody.subtotal
  }
  return Cart.findByIdAndUpdate(reqParams.cart_id, updateCartQuantity).then((cart, error) => {
    if(error) {
      return 'Unexpected Error'
    } else {
      return 'Your changes have been successfully saved!'
    }
  })
}

const deleteCart = (req, res) => {
  return Cart.findByIdAndDelete(req.cart_id).then(result => {
    console.log(result)
    return result
  })
}

const getAllCarts = () => {
  return Cart.find({}).then(result => {
    return result
  })
}

module.exports = {
  addProductsToCart,
  updateCart,
  deleteCart,
  getAllCarts
}