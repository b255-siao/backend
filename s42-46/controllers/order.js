const Product = require("../models/Product")
const User = require("../models/User")

// User checkout (Create Order) 
const checkout = async (data) => {
  let assignUser = await User.findById(data.user_id)
    .then(
      user => {
        // console.log(user.ordered_product)
        user.ordered_product.push({
          product_id: data.product_id,
          total_amount: data.total_amount,
          products: [{
              product_id: data.product_id,
              product_name: data.product_name,
              quantity: data.quantity
          }]
        })
    return user.save().then((user, error) => {
      if (error) {
        return false
      } else {
        return 'Order successful'
      }
    })
  })

  let assignProduct = await Product.findById(data.product_id)
    .then(
      product => {
        // console.log(product.user_orders)
        product.user_orders.push({
          user_id: data.user_id
        })
      return product.save().then((product, error) => {
        if (error) {
          return false
        } else {
          return true
        }
      })
  })

  if (assignUser && assignProduct) {
    return true
  } else {
    return false  
  }
}


module.exports = {
  checkout
}