require('dotenv').config()

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRouter = require("./routes/user")
const productRouter = require("./routes/product")
const orderRouter = require("./routes/order")
const cartRouter = require("./routes/cart")

const connectDB = require('./db/connectionUrl')

const app = express()

connectDB(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
  console.log('Now connected to MongoDB Atlas')
});

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use("/api/v1/users", userRouter)
app.use("/api/v1/products", productRouter)
app.use("/api/v1/orders", orderRouter)
app.use("/api/v1/cart", cartRouter)

if(require.main === module) {
  app.listen(process.env.PORT || 5000, () => {
    console.log(`API is now online on port ${process.env.PORT || 5000}`)
  });
};

module.exports = app;