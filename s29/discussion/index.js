// CRUD operations

// Insert Document (CREATE)

/*
  Syntax:
    Insert One Document:
      db.collectionName.insertOne({
        "fieldA": "valueA",
        "fieldB": "valueB"
      });

    Insert Many Documents:
      db.collectionName.insertMany([
        {
          "fieldA": "valueA",
          "fieldB": "valueB"
        },
        {
          "fieldA": "valueA",
          "fieldB": "valueB",
        }
      ]);
*/

db.users.insertOne({
  "firstName": "Jane",
  "lastName": "Doe",
  "age": 21,
  "email": "janedoe@mail.com",
  "company": "none"
});

db.users.insertMany([
  {
    "firstName": "Stephen",
    "lastName": "Hawking",
    "age": 76,
    "email": "stephenhawking@mail.com",
    "company": "none"
  },
  {
    "firstName": "Neil",
    "lastName": "Armstrong",
    "age": 82,
    "email": "neilarmstrong@mail.com",
    "company": "none"
  }
]);

// Find Documents (READ/RETRIEVE):
db.users.find(); // This will retrieve all the documents in the collection

db.users.find({
  "firstName": "Jane"
});

db.users.findOne({}); // Returns the first document in our collection

db.users.findOne({
  "firstName": "Stephen"
})

// Update Documents (UPDATE):
db.users.insertOne({
  "firstName": "Test",
  "lastName": "Test",
  "age": 76,
  "email": "test@mail.com",
  "company": "Test"
});

// Updating One Document:
db.users.updateOne(
  {
  "firstName": "Test"
  },
  {
    $set: {
      "firstName": "Bill",
      "lastName": "Gates",
      "age": 65,
      "email": "billgates@mail.com",
      "company": "Microsoft",
      "status": "active"
    }
  }
);

// Removing a field:
db.users.updateOne(
  {
    "firstName": "Bill"
  },
  {
    $unset: {
      "status": "active"
    }
  }
);

// Updating Multiple Documents:
db.users.updateMany(
  {
    "company": "none"
  },
  {
    $set: {
      "company": "HR"
    }
  }
);

db.users.updateOne(
  {},
  {
    $set: {
      "company": "operations"
    }
  }
);

// Deleting Documents (DELETE):
db.users.insertOne({
  "firstName": "Test"
});

db.users.deleteOne({
  "firstName": 'Test'
});

db.users.deleteMany({
  "company": "comp"
});

db.users.deleteMany({});

db.courses.insertMany([
  {
    "name": "Javascript101",
    "price": 5000,
    "description":"Introduction to Javascript",
    "isActive": true,
  },
  {
    "name": "HTML 101",
    "price": 2000,
    "description":"Introduction to HTML",
    "isActive": true,
  },
  {
    "name": "CSS 101",
    "price": 2500,
    "description":"Introduction to CSS",
    "isActive": true,
  }
]);

// [SECTION] Comparison Operators

// $gt/$gte operator:
db.users.find({age: {$gt: 50}}).pretty();

db.users.find({age: {$gte: 50}}).pretty();


// $lt/$lte operator:
db.users.find({age: {$lt: 50}});

db.users.find({age: {$lte: 50}});


// $ne operator:
// Allows us to find documents that have field number values not equal to a specified value.
db.users.find({age: {$ne: 82}});


// $in operator:
// Allows us to find documents with specific match criteria one field using different values.
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.courses.find({name: {$in: ["HTML 101", "CSS 101"]}});


// [SECTION] Logical Query Operators

// $or
// Allows us to find documents that match a single criteria from multiple provided search criteria
db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 70}}]});


// $and operator:
// Allows us to find documents matching multiple criteria in a single field
db.users.find({ $and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});


// [SECTION] Field Projection
/*
  - Retrieving documents are common operations that we do by default. MongoDB queries return the whole document as a response.
  - When dealing with complex data structures, there might be instances when fields are not useful for the query we are trying to accomplish
  - To help with readability of values returned, we can include/exclude fields from the response
*/

// Exlusion: 0, Inclusion: 1

db.users.find(
  {
    firstName: "Jane"
  },
  {
    firstName: 1,
    lastName: 1,
    email: 1
  }
);

db.users.find(
  {
    firstName: "Jane"
  },
  {
    company: 0,
    email: 0
  }
);


// Suppressing the ID field
/*
  - Allows us to exclude the "_id" field when retrieving documents
  - When using field projection, field inclusion and exclusion may not be used at the same time
  - Excluding the "_id" field is the only exception to this rule
*/
db.users.find(
  {
    firstName: "Jane"
  },
  {
    firstName: 1,
    lastName: 1,
    company: 1,
    _id: 0   
  }
)


db.users.insert({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
      phone: "87654321",
      email: "janedoe@gmail.com"
  },
  courses: [ "CSS", "Javascript", "Python" ],
  department: "none"
});

db.users.find(
  {
    firstName: "John"
  },
  {
    firstName: 1,
    lastName: 1,
    "contact.phone": 1
  }
)

// suppressing specific fields in embedded documents
db.users.find(
  {
    firstName: "John"
  },
  {
    "contact.phone": 0
  }
);

db.users.insert({
  namearr: [
      {
          namea: "juan"
      },
      {
          nameb: "tamad"
      }
  ]
});

// Project specific array elements in the returned array
// The $slice operator allows us to retrieve only 1 element that matches the search criteria
db.users.find(
  {
    "namearr":
      {
        namea: "juan"
      }
  },
  {
    namearr: {$slice: 1}
  }
);

// [SECTION] Evaluation Query Operators
// $regex operator
/*
  - Allows us to find documents that match a specific string pattern using regular expressions
*/
db.users.find({firstName: {$regex: "N"}});

// Case Insensitive Query
db.users.find({firstName: {$regex: "j", $options: "$i"}})