db.users.find(
  {
    $or: [{firstName: {$regex: "S"}}, {lastName: {$regex: "D"}}]
  }, 
  {
    firstName: 1,
    lastName: 1,
    _id: 1
  }
);

db.users.find(
  {
    $and: [{company: "none"}, {age: {$gte: 70}}]
  }
);

db.users.find(
  {
    $and: [{firstName: {$regex: "e"}}, {age: {$lte: 30}}]
  }
);