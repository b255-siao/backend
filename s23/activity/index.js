console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object


let trainer = {
  age: 10,
  friends: {
    hoenn: ['May', 'Max'],
    kanto: ['Brock', 'Misty']
  },
  name: 'Ash Ketchum',
  pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
  talk: function() {
    return this.pokemon[0] + '! I choose you!'
  }
};
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method');
console.log(trainer.talk());

let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level*2;
  this.attack = level;

  this.tackle = function(target) {
    let targetHealth = target.health - this.attack;
    if(targetHealth <= 0) {
      target.health = targetHealth;
      console.log(this.name + ' tackled ' + target.name);
      console.log(target.name + "'s health is reduced to " + targetHealth);
      target.faint();
      console.log(target);
    } else {
      target.health = targetHealth;
      console.log(this.name + ' tackled ' + target.name);
      console.log(target.name + "'s health is reduced to " + targetHealth);
      console.log(target);
    }
  };

  this.faint = function() {
    console.log(this.name + ' fainted');
  };
};
geodude.tackle(pikachu);
mewtwo.tackle(geodude);
// pikachu.tackle(geodude);
// geodude.tackle(mewtwo);


//Do not modify
//For exporting to test.js
try{
  module.exports = {
    trainer,
		Pokemon 
	}
} catch(err) {

}
