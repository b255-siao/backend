let http = require("http");

// mock database
let directory = [
  {
    "name": "Brandon",
    "email": "brandon@mail.com"
  },
  {
    "name": "Jobert",
    "email": "jobert@mail.com"
  }
];

http.createServer(function(request, response){
  // route for returning all items upon receiving a GET request
  if(request.url == '/users' && request.method == 'GET') {
    // sets response output to JSON data type
    response.writeHead(200, {'Content-Type': 'application/json'});

    // input has to be data type STRING hence the JSON.stringify() method
    // This string input will be converted to desired output data type which has been sent to JSON
    // This is done because requests and responses sent between client and Node.js server requires the information send 
    // and received as a stringified JSON
    response.write(JSON.stringify(directory));
    response.end();
  }

  // a request object contains several parts
    // - Headers - contains information aboyt the request context/content like what is the data type
    // - Body- contains the actual information being sent with the request
  if(request.url == '/users' && request.method == 'POST') {
    let requestBody = '';

    // data is received from the client and is processed in the data strean
    // the information provided from the request object enters a sequence called "data" the code below will be triggered
    request.on('data', function(data){
      // assigns the data retrieved from the data stream to the requestBody 
      requestBody += data;
    });

    request.on('end', function(){
      // we need this to be of data type JSON to access its properties
      console.log(typeof requestBody);
  
      // converts the string requestBody to JSON
      requestBody = JSON.parse(requestBody);
  
      // create a new object representing the new mock database record
      let newUser = {
        "name": requestBody.name,
        "email": requestBody.email
      };

      directory.push(newUser);
      console.log(directory);

      response.writeHead(200, {'Content-Type': 'application/json'});
      response.write(JSON.stringify(newUser));
      response.end();
    })
  };
}).listen(4000);

console.log('Server running at localhost:4000');