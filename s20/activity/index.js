// console.log("Hello World");

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

        // If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the counter number  value is divisible by 5, print/console log the counter number.


      
      // Add code here
      
      
function printNumbers() {
  let count = Number(prompt('Give me a number: '));
  
  console.log('The number you provided is ' + count + '.');
  while(count > 0) {
    count--;

    if(count <= 50) {
      console.log('The current value is at ' + count + '. Terminating the loop.');
      break;
    };
    
    if(count % 10 === 0) {
      console.log('The number is divisible by 10. Skipping the number');
      continue;
    };
    
    if(count % 5 === 0) {
      console.log(count);
    };
  }
} 
printNumbers()

function removeVowel(string) {
  let filteredString = '';
  
  for(let i = 0; i < string.length; i++) {
    if(
      string[i].toLowerCase() != 'a' &&
      string[i].toLowerCase() != 'e' &&
      string[i].toLowerCase() != 'i' &&
      string[i].toLowerCase() != 'o' &&
      string[i].toLowerCase() != 'u'
    ) {
      filteredString += string[i];
    }
  }
  console.log(filteredString)
}
let string = 'supercalifragilisticexpialidocious';
console.log(string);
console.log(removeVowel(string));


//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}
