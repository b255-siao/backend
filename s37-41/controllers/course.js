const Course = require("../models/Course");

// Create a new course
/*
  Steps:
  1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
  2. Save the new Course to the database
*/

module.exports.addCourse = (reqBody) => {
  // Create a variable "newCourse" and instantiates a new "Course" object using the mongoose model
  // if (isAdmin === false) {
  //   return Promise.resolve(false);
  // }

  let newCourse = new Course({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  })

  // Saves the created object to our database
  return newCourse.save().then((course, error) => {
    // Course creation failed
    if(error) {
      return false;
    
    // Course creation successful
    } else {
      return true;
    }
  })
}

// Retrieve all courses
/*
  Steps:
  1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () => {
  return Course.find({}).then(result => {
    return result
  })
};

// Retrieve all ACTIVE courses
/*
  Steps:
  1. Retrieve all the courses from the db with the property of "isActive" to true
*/
module.exports.getAllActive = () => {
  return Course.find({isActive: true}).then(result => {
    return result
  })
};

// Retrieving a specific course
/*
  Steps:
  1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId).then(result => {
    return result
  })
};

// Update a course
/*
  Steps:
  1. Create a variable "updateCourse" which will contain the information retrieved from the request body
  2. Find and update the course using the course ID retrieved from the params property and the variable "updateCourse" containing the information from the request body
*/

// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {
  // Specify the fields/properties of the document to be updated
  let updateCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  }

  return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
    // Course is not updated
    if(error) {
      return false
    // Course updated successfully
    } else {
      return true
    }
  })
}

module.exports.patchCourse = (reqParams) => {
  let updateActiveField = {
    isActive: false
  }

  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
    if(error) {
      return false
    } else {
      return true
    }
  })
}